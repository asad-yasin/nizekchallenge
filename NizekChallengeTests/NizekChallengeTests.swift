//
//  NizekChallengeTests.swift
//  NizekChallengeTests
//
//  Created by Rana Asad on 15/09/2022.
//

import XCTest
@testable import NizekChallenge
class NizekChallengeTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLoginController() throws {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: LoginViewController.self)
        vc.startViewLifecycle()
        XCTAssertNotNil(vc.managedObjectContext)
        XCTAssertNotNil(vc.usernameTextField)
        XCTAssertNotNil(vc.passwordTextField)
    }
    func testSignUpController() throws {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: SignupViewController.self)
        vc.startViewLifecycle()
        XCTAssertNotNil(vc.managedObjectContext)
        XCTAssertNotNil(vc.usernameTextField)
        XCTAssertNotNil(vc.fullnameTextField)
        XCTAssertNotNil(vc.passwordTextField)
    }
    func testCustomView() throws {
        let buttonStyle = SimpleButton(title: "Login")
        let customButton = CustomizableGenericButton(buttonStyle: buttonStyle)
        
    }
    func testPerformanceExample() throws {
        
    }

}
