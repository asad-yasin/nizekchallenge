//
//  ButtonStyle.swift
//  NizekChallenge
//
//  Created by Rana Asad on 14/09/2022.
//

import Foundation
class SimpleButton {
    var type: ButtonType = .SimpleButton
    var title: String
    init(title: String) {
        self.title = title
    }
}
class SubTitleButton: SimpleButton {
    var subTitle: String
    init(title: String, subTitle: String) {
        self.subTitle = subTitle
        super.init(title: title)
        self.type = .SubtitleButton
    }
}
class IconButton: SimpleButton {
    var subTitle: String
    var icon: String
    init(title: String, subTitle: String, icon: String) {
        self.subTitle = subTitle
        self.icon = icon
        super.init(title: title)
        self.type = .IconButton
    }
}
enum ButtonType {
    case SimpleButton
    case SubtitleButton
    case IconButton
}
