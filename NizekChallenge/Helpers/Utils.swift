//
//  Utils.swift
//  NizekChallenge
//
//  Created by Rana Asad on 15/09/2022.
//

import Foundation
import UIKit

class Utils {
    static let shared = Utils()
    var enterBackgroundTimeStamp: Int64 = 0
    var enterForgroundTimeStamp: Int64 = 0
    func decoratTextField(textField: UITextField) {
        textField.layer.borderColor = ThemeManager.theme.colorPrimary.cgColor
        textField.layer.cornerRadius = 4
        textField.layer.borderWidth = 1
    }
    func showAlertWrapper(viewController: UIViewController,alertTitle: String, alertMessage: String) {
        let alertController = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert);
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil);
        alertController.addAction(okAction);
        viewController.present(alertController, animated: true, completion: nil)
    }
}
