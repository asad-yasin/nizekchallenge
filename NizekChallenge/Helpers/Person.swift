//
//  CoreDataManager.swift
//  NizekChallenge
//
//  Created by Rana Asad on 15/09/2022.
//

import Foundation
import CoreData
extension Person {
    class func fetchAll(in managedObjectContext: NSManagedObjectContext) -> [Person] {
        // Helpers
        var people: [Person] = []
        
        // Create Fetch Request
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            // Perform Fetch Request
            people = try managedObjectContext.fetch(fetchRequest)
        } catch {
            print("Unable to Fetch Workouts, (\(error))")
        }
        
        return people
    }
    class func addPerson(in managedObjectContext: NSManagedObjectContext,fullname: String, username: String, password: String) {
        let newPerosn = Person(context: managedObjectContext)
        newPerosn.setValue(username, forKey: "username")
        newPerosn.setValue(fullname, forKey: "fullname")
        newPerosn.setValue(password, forKey: "password")
        do {
            try managedObjectContext.save()
        } catch let error as NSError {
            print("Unresolved error \(error), \(error.userInfo)")
        }
    }
}
