//
//  Theme.swift
//  NizekChallenge
//
//  Created by Rana Asad on 14/09/2022.
//

import Foundation
import UIKit

class Theme: NSObject {
    let colorPrimary: UIColor
    let colorText: UIColor
    
    private init(colorPrimary: UIColor, colorText: UIColor) {
        self.colorPrimary = colorPrimary
        self.colorText = colorText
    }
    private func getColor(colorName: String) -> UIColor {
        return UIColor(named: colorName)!
    }
    static let indigo = Theme(colorPrimary: UIColor(named: ColorsName.indigoColorsName.colorPrimary)! , colorText: UIColor(named: ColorsName.indigoColorsName.colorText)!)
    static let teal = Theme(colorPrimary: UIColor(named: ColorsName.tealColorsName.colorPrimary)!, colorText: UIColor(named: ColorsName.tealColorsName.colorText)!)
    
}

class ColorsName: NSObject {
    let colorPrimary: String
    let colorText: String
    private init(colorPrimary: String, colorText: String) {
        self.colorPrimary = colorPrimary
        self.colorText = colorText
    }
    static let indigoColorsName = ColorsName(colorPrimary: "IndigoPrimaryColor", colorText: "IndigoTextColor")
    static let tealColorsName = ColorsName(colorPrimary: "TealPrimaryColor", colorText: "TealTextColor")
}
