//
//  ViewController.swift
//  NizekChallenge
//
//  Created by Rana Asad on 14/09/2022.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func signupButtonClicked(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: SignupViewController.self)
        present(vc, animated: true)
    }
    
    @IBAction func demoButtonClicked(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: DemoViewController.self)
        present(vc, animated: true)
    }
    
}

