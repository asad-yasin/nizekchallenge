//
//  LoginViewController.swift
//  NizekChallenge
//
//  Created by Rana Asad on 15/09/2022.
//

import UIKit
import CoreData
class LoginViewController: UIViewController {
    @IBOutlet weak var usernameTextField: TextField!
    @IBOutlet weak var passwordTextField: TextField!
    
    var managedObjectContext: NSManagedObjectContext?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Utils.shared.decoratTextField(textField: usernameTextField)
        Utils.shared.decoratTextField(textField: passwordTextField)
        addLoginButton()
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        managedObjectContext = appDelegate.persistentContainer.viewContext
        // Do any additional setup after loading the view.
    }
    /// produce the sign up button and add it to view.
    private func addLoginButton() {
        let simpleButtonStyle = SimpleButton(title: "Login")
        let simpleButton = CustomizableGenericButton(buttonStyle: simpleButtonStyle)
        simpleButton.isFilled = false
        simpleButton.borderWidth = 1
        simpleButton.cornerRadius = 4
        view.addSubview(simpleButton)
        
        simpleButton.translatesAutoresizingMaskIntoConstraints = false
        let simpleHorConstraint = NSLayoutConstraint(item: simpleButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let simpleVerConstraint = NSLayoutConstraint(item: simpleButton, attribute: .top, relatedBy: .equal, toItem: passwordTextField, attribute: .bottom, multiplier: 1, constant: 50)
        let simpleWidthConstraint = NSLayoutConstraint(item: simpleButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 140)
        let simpleHeightConstraint = NSLayoutConstraint(item: simpleButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        view.addConstraints([simpleHorConstraint,simpleVerConstraint,simpleWidthConstraint,simpleHeightConstraint])
        addGesture(view: simpleButton)
    }
    private func login() {
        guard let username = usernameTextField.text else {
            return
        }
        guard let password = passwordTextField.text else {
            return
        }
        guard let managedObjectContext = managedObjectContext else {
            return
        }
        
        if username.isEmpty || password.isEmpty {
            Utils.shared.showAlertWrapper(viewController: self, alertTitle: "Oops", alertMessage: "Please fill all the fields.")
        } else {
            let people = Person.fetchAll(in: managedObjectContext)
            if people.contains(where: { $0.username == username }) {
                let person = people.first(where: { $0.username == username })
                guard let per = person else {
                    return
                }
                if per.password == password {
                    guard let fullname = per.fullname else {
                        return
                    }
                    usernameTextField.text = ""
                    passwordTextField.text = ""
                    let vc = AppStoryboard.Main.viewController(viewControllerClass: HomeViewController.self)
                    vc.modalPresentationStyle = .fullScreen
                    vc.fullname = fullname
                    self.present(vc, animated: true)
                } else {
                    Utils.shared.showAlertWrapper(viewController: self, alertTitle: "Oops", alertMessage: "Invalid Password.")
                }
            } else {
                Utils.shared.showAlertWrapper(viewController: self, alertTitle: "Oops", alertMessage: "User not found.")
            }
        }
    }
    
    private func addGesture(view: UIView) {
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        sender.view?.showAnimation {[weak self] in
            self?.login()
        }
    }
    
}
extension UIViewController {
    
    func startViewLifecycle() {
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
}
