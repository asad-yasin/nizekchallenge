//
//  SignupViewController.swift
//  NizekChallenge
//
//  Created by Rana Asad on 15/09/2022.
//

import UIKit
import CoreData

class SignupViewController: UIViewController {
    @IBOutlet weak var usernameTextField: TextField!
    @IBOutlet weak var fullnameTextField: TextField!
    @IBOutlet weak var passwordTextField: TextField!
    @IBOutlet weak var loginButton: UIButton!
    
    var managedObjectContext: NSManagedObjectContext?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewDecorator()
        addSignupButton()
        
        guard let appDelegate =
           UIApplication.shared.delegate as? AppDelegate else {
           return
         }
        managedObjectContext = appDelegate.persistentContainer.viewContext
        // Do any additional setup after loading the view.
    }
    /// produce the sign up button and add it to view.
    private func addSignupButton() {
        let simpleButtonStyle = SimpleButton(title: "Signup")
        let simpleButton = CustomizableGenericButton(buttonStyle: simpleButtonStyle)
        simpleButton.isFilled = false
        simpleButton.borderWidth = 1
        simpleButton.cornerRadius = 4
        view.addSubview(simpleButton)
        
        simpleButton.translatesAutoresizingMaskIntoConstraints = false
        let simpleHorConstraint = NSLayoutConstraint(item: simpleButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let simpleVerConstraint = NSLayoutConstraint(item: simpleButton, attribute: .top, relatedBy: .equal, toItem: passwordTextField, attribute: .bottom, multiplier: 1, constant: 50)
        let simpleWidthConstraint = NSLayoutConstraint(item: simpleButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 140)
        let simpleHeightConstraint = NSLayoutConstraint(item: simpleButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
        view.addConstraints([simpleHorConstraint,simpleVerConstraint,simpleWidthConstraint,simpleHeightConstraint])
        addGesture(view: simpleButton)
    }
    /// Add the Decoration to the Views.
    private func viewDecorator() {
        Utils.shared.decoratTextField(textField: usernameTextField)
        Utils.shared.decoratTextField(textField: fullnameTextField)
        Utils.shared.decoratTextField(textField: passwordTextField)
        
    }
    /// Add the Tap gesture to view.
    private func addGesture(view: UIView) {
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(getGesture())
    }
    private func getGesture() -> UITapGestureRecognizer {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(viewTap(sender:)))
        return gesture
    }
    private func signUp() {
        guard let username = usernameTextField.text else {
            return
        }
        guard let fullname = fullnameTextField.text else {
            return
        }
        guard let password = passwordTextField.text else {
            return
        }
        guard let managedObjectContext = managedObjectContext else {
            return
        }

        if username.isEmpty || password.isEmpty || fullname.isEmpty || username.count < 5 || password.count < 5 {
            Utils.shared.showAlertWrapper(viewController: self, alertTitle: "Oops", alertMessage: "All fields required. Username and Password must be greater than 5 letters.")
        } else {
            let people = Person.fetchAll(in: managedObjectContext)
            
            if !people.contains(where: { $0.username == username }) {
                Person.addPerson(in: managedObjectContext, fullname: fullname, username: username, password: password)
            } else {
                Utils.shared.showAlertWrapper(viewController: self, alertTitle: "Oops", alertMessage: "This user is already exist. Please go to login.")
            }
        }
    }
    @objc func viewTap(sender: UITapGestureRecognizer) {
        sender.view?.showAnimation {[weak self] in
            self?.signUp()
        }
        
    }
    
    @IBAction func loginButtonClicked(_ sender: Any) {
        let vc = AppStoryboard.Main.viewController(viewControllerClass: LoginViewController.self)
        present(vc, animated: true)
    }
    
}
