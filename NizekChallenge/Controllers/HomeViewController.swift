//
//  HomeViewController.swift
//  NizekChallenge
//
//  Created by Rana Asad on 15/09/2022.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var nameLabel: UILabel!
    
    public var fullname = ""
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = fullname
        Utils.shared.enterForgroundTimeStamp = Int64(Date().timeIntervalSince1970 * 1000)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        scheduledTimerWithTimeInterval()
        // Do any additional setup after loading the view.
    }
    func scheduledTimerWithTimeInterval(){
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: {[weak self] _ in
            self?.checkForeground()
        })
    }
    @objc func checkForeground() {
        if Utils.shared.enterForgroundTimeStamp != 0 {
            let currentMillis = Int64(Date().timeIntervalSince1970 * 1000)
            let difference = currentMillis - Utils.shared.enterForgroundTimeStamp
            let seconds = difference / 1000
            if seconds > 30 {
                self.showAlert()
            } else {
                Utils.shared.enterForgroundTimeStamp = currentMillis
            }
        }
    }
    @objc func willEnterForeground() {
        Utils.shared.enterForgroundTimeStamp = Int64(Date().timeIntervalSince1970 * 1000)
        if Utils.shared.enterBackgroundTimeStamp != 0 {
            let currentMillis = Int64(Date().timeIntervalSince1970 * 1000)
            let difference = currentMillis - Utils.shared.enterBackgroundTimeStamp
            Utils.shared.enterBackgroundTimeStamp = 0
            let seconds = difference / 1000
            if seconds > 10 {
                showAlert()
            }
        }
    }
    private func showAlert() {
        let alertController = UIAlertController(title: "Warning", message: "Login session is expired. Please login again.", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: {[weak self] _ in
            self?.timer.invalidate()
            self?.dismiss(animated: true)
        })
        alertController.addAction(okAction);
        self.present(alertController, animated: true, completion: nil)
    }
    
}
