//
//  DemoViewController.swift
//  NizekChallenge
//
//  Created by Rana Asad on 15/09/2022.
//

import UIKit

class DemoViewController: UIViewController {
    
    private let buttonWidth: CGFloat = 140.0
    private let buttonHeight: CGFloat = 50.0
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubView()
        // Do any additional setup after loading the view.
    }
    private func addSubView() {
        /// Adding the Icon Button Demo.
        let iconButtonStyle = IconButton(title: "Title", subTitle: "SubTitle", icon: Icons.checkIcon)
        let iconButton = CustomizableGenericButton(buttonStyle: iconButtonStyle)
        iconButton.isFilled = false
        iconButton.borderWidth = 1
        iconButton.cornerRadius = 4
        view.addSubview(iconButton)
        
        iconButton.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = NSLayoutConstraint(item: iconButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let verticalConstraint = NSLayoutConstraint(item: iconButton, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)
        let widthConstraint = NSLayoutConstraint(item: iconButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonWidth)
        let heightConstraint = NSLayoutConstraint(item: iconButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonHeight)
        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
        
        /// Adding the Subtitle Button Demo.
        
        let subTitleButtonStyle = SubTitleButton(title: "Title", subTitle: "SubTitle")
        let subTitleButton = CustomizableGenericButton(buttonStyle: subTitleButtonStyle)
        subTitleButton.isFilled = false
        subTitleButton.borderWidth = 1
        subTitleButton.cornerRadius = 4
        view.addSubview(subTitleButton)
        
        subTitleButton.translatesAutoresizingMaskIntoConstraints = false
        let subHorConstraint = NSLayoutConstraint(item: subTitleButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let subVerConstraint = NSLayoutConstraint(item: subTitleButton, attribute: .top, relatedBy: .equal, toItem: iconButton, attribute: .bottom, multiplier: 1, constant: 50)
        let subWidthConstraint = NSLayoutConstraint(item: subTitleButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonWidth)
        let subHeightConstraint = NSLayoutConstraint(item: subTitleButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonHeight)
        view.addConstraints([subHorConstraint,subVerConstraint,subWidthConstraint,subHeightConstraint])
        
        /// Adding the Simple Button Demo.
        
        let simpleButtonStyle = SimpleButton(title: "Title")
        let simpleButton = CustomizableGenericButton(buttonStyle: simpleButtonStyle)
        simpleButton.isFilled = false
        simpleButton.borderWidth = 1
        simpleButton.cornerRadius = 4
        view.addSubview(simpleButton)
        
        simpleButton.translatesAutoresizingMaskIntoConstraints = false
        let simpleHorConstraint = NSLayoutConstraint(item: simpleButton, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)
        let simpleVerConstraint = NSLayoutConstraint(item: simpleButton, attribute: .top, relatedBy: .equal, toItem: subTitleButton, attribute: .bottom, multiplier: 1, constant: 50)
        let simpleWidthConstraint = NSLayoutConstraint(item: simpleButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonWidth)
        let simpleHeightConstraint = NSLayoutConstraint(item: simpleButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: buttonHeight)
        view.addConstraints([simpleHorConstraint,simpleVerConstraint,simpleWidthConstraint,simpleHeightConstraint])
    }

}
