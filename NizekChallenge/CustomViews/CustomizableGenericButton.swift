//
//  CustomizableGenricButton.swift
//  NizekChallenge
//
//  Created by Rana Asad on 14/09/2022.
//

import Foundation
import UIKit
class CustomizableGenericButton: UIView {
    var borderWidth: CGFloat = 1.0
    var isFilled: Bool = true
    var cornerRadius: CGFloat = 0.0
    var fontSize: CGFloat = 15.0
    var iconSize: CGFloat = 24.0
    
    private var buttonStyle: SimpleButton
    
    required init(buttonStyle: SimpleButton) {
        self.buttonStyle = buttonStyle
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        self.layer.cornerRadius = cornerRadius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = ThemeManager.theme.colorPrimary.cgColor
        self.backgroundColor = isFilled ? ThemeManager.theme.colorPrimary : .clear
        setup()
    }
    /// Setup the Different Button Varaitions.
    private func setup() {
        switch buttonStyle.type {
        case .SimpleButton:
            setupSimpleButton()
            break
        case .SubtitleButton:
            setupSubTitleButton()
            break
        case .IconButton:
            setupIconButton()
            break
        }
    }
    /// Produce the Simple Button varaition.
    private func setupSimpleButton() {
        let titleLabel = getLabel(text: buttonStyle.title, isTitle: true)
        self.addSubview(titleLabel)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let xConstraint = NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yConstraint = NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        self.addConstraints([xConstraint,yConstraint])
    }
    /// Produce the Title and SubTitle Label.
    /// Add it to the View.
    private func addTitleAndSubtitle(title: String, subTitle: String) {
        let titleLabel = getLabel(text: title, isTitle: true)
        let subTitleLabel = getLabel(text: subTitle, isTitle: false)
        let dividerView = UIView()
        
        self.addSubview(dividerView)
        self.addSubview(titleLabel)
        self.addSubview(subTitleLabel)
        
        dividerView.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        subTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        // Adding the Divider View Constraints
        let xDividerConstraint = NSLayoutConstraint(item: dividerView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yDividerConstraint = NSLayoutConstraint(item: dividerView, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0)
        let heightDividerConstraint = NSLayoutConstraint(item: dividerView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1)
        let widthDividerConstraint = NSLayoutConstraint(item: dividerView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 1)
        
        // Adding the Title Label Constraints
        let xTitleConstraint = NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let yTitleConstraint = NSLayoutConstraint(item: titleLabel, attribute: .bottom, relatedBy: .equal, toItem: dividerView, attribute: .top, multiplier: 1, constant: 1)
        
        // Adding the Sub Title Constraints
        let xSubTitleConstraint = NSLayoutConstraint(item: subTitleLabel, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0)
        let ySubTitleConstraint = NSLayoutConstraint(item: subTitleLabel, attribute: .top, relatedBy: .equal, toItem: dividerView, attribute: .bottom, multiplier: 1, constant: 1)
        
        self.addConstraints([xDividerConstraint,yDividerConstraint,widthDividerConstraint,heightDividerConstraint,xTitleConstraint,yTitleConstraint,xSubTitleConstraint,ySubTitleConstraint])
        
    }
    /// Produce the Title and Subtitle Button Varaition.
    private func setupSubTitleButton() {
        if let buttonStyle = self.buttonStyle as? SubTitleButton {
            addTitleAndSubtitle(title: buttonStyle.title, subTitle: buttonStyle.subTitle)
        } else {
            fatalError("Invalid Type of Button.")
        }
    }
    /// Produce the icon Button Variation.
    private func setupIconButton() {
        if let buttonStyle = self.buttonStyle as? IconButton {
            let iconName = buttonStyle.icon
            addTitleAndSubtitle(title: buttonStyle.title, subTitle: buttonStyle.subTitle)
            let iconImage = UIImageView(image: UIImage(named: iconName))
            
            self.addSubview(iconImage)
            
            iconImage.translatesAutoresizingMaskIntoConstraints = false
            
            let leadingIconConstraint = NSLayoutConstraint(item: iconImage, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 5)
            let yIconConstraint = NSLayoutConstraint(item: iconImage, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 1)
            let heightIconConstraint = NSLayoutConstraint(item: iconImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: iconSize)
            let widthIconConstraint = NSLayoutConstraint(item: iconImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: iconSize)
            
            self.addConstraints([leadingIconConstraint,yIconConstraint,widthIconConstraint,heightIconConstraint])
        } else {
            fatalError("Invalid Type of Button.")
        }
    }
    /// Produce a label.
    ///
    /// - Parameters:
    ///     - text: The text of the label.
    ///     - isTitle: If the label is going to behave as Title then this must be true.
    ///
    /// - Returns: A Label.
    private func getLabel(text: String,isTitle: Bool) -> UILabel {
        let label = UILabel()
        label.text = text
        label.font = isTitle ? UIFont.boldSystemFont(ofSize: fontSize) : UIFont.systemFont(ofSize: fontSize)
        label.textColor = isFilled ? ThemeManager.theme.colorText : ThemeManager.theme.colorPrimary
        return label
    }
}
